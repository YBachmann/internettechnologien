<?php
//Einfache version ohne Framework, berücksichtigt das die meißten Browser kein PUT und DELETE unterstützen


class verkauf
{
   private $db;

   public function __construct()
   {
      //***TODO*** --> insert your database connection:
      $this->db = new mysqli("localhost","allgold", "allgold");

      if (mysqli_connect_errno())
      {
      	die("error while connection to database!:".mysqli_connect_error());
      }

      $this->db->select_db("allgold_erp");

      if($this->db->errno)
      {
      	die ($this->db->error);
      }
   }

   //R ead
   //get all products available at station 
   public function find_availableProducts($stationID){
    $allProducts = array();
    $stmt = "SELECT p.ID, name, currentAmount from inventory i JOIN products p ON i.productID = p.ID AND stationID ='".$stationID."' WHERE currentAmount > 0 ORDER BY p.ID;";
    $result = $this->db->query($stmt);

    if(empty($result))
        {
           return "your statement: ".$stmt."<br /> received result:".$result;
        }

      while ($row = $result->fetch_assoc()) 
      {
        $allProducts[] = $row;
      }

      return $allProducts;
   	  //return $row = $result->fetch_assoc(); 
   }

   public function captureSale($data){
      $stationID=$data["stationID"];
      $productnames = array();
      // get names and current amount of products from specified station
      $stmt = "SELECT name, currentAmount FROM products p JOIN inventory i ON i.productID = p.ID AND stationID ='".$stationID."' ORDER BY p.ID;";
      $result = $this->db->query($stmt);
      while ($row = $result->fetch_assoc()) 
      {
         // write all products and amount in array
         $productnames[] = $row;
      }
      $productid= 1;
      
      for ($i = 0; $i < sizeof($productnames); $i++) {
         
         $productnamejson = $productnames[$i];
         $productnameVal = $productnamejson["name"];
         if (array_key_exists($productnameVal, $data)){
            $amount = $data[$productnameVal];
            if ($amount > 0){
               // add sale to database
               $stmt = "INSERT INTO sales (productID, stationID, userID, amount) VALUES ('".$productid."', '".$stationID."', 2, '".$amount."');";
               $this->db->query($stmt);
               // adjust inventory in database
               $inventoryamount = $productnamejson["currentAmount"];
               $inventoryamount = $inventoryamount - $amount;
               $stmt = "UPDATE inventory SET currentAmount = '".$inventoryamount."' WHERE stationID = '".$stationID."' AND productID = '".$productid."';";
               $this->db->query($stmt);

            }
         }
         $productid++;
      }
      return ("Verkauf erfolgreich!");
   }
}
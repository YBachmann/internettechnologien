<?php
 
// include necessary classes 
include('verkauf.php');


$verkauf = new verkauf();
$data = array_merge($_GET, $_POST);
$method = $data['action'];
$retlnk = '<br> <a href="verkauf.html"> zur&uuml;ck zur Verkaufsseite </a>';

switch($method){
    case "GET":
        if(!empty($data['stationID']))
        {
            $sql = $verkauf->find_availableProducts($data['stationID']);
            header('Content-type: application/json; charset=utf-8'); 
            echo json_encode($sql);
            break;
        }
    case 'POST':
        $sql = $verkauf->captureSale($data); 
        echo "Antwort: ".$sql.$retlnk;
        break;
}
<!DOCTYPE HTML>
<html>

<head>
    <title>Allgold Webcenter</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" type="text/css" href="../css/main.css" />
    <script type="text/javascript" src="../js/verkauf.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
        integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
</head>

<body>
    <div class="grid-container">
        <div class="item1">
            <div class="headergrid text-center">
                <div class="headerheadline">
                    <h1>
                        Allgold
                    </h1>
                </div>
                <div class="header_ataboutwriting">
                    <h2>
                        Die zarte Seite des Allgäus
                    </h2>
                </div>
            </div>
        </div>

        <nav class="navbar navbar-expand-sm navbar-dark bg-dark">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="../index.html">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../Lieferant/lieferant.html">Lieferant</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../Verkauf/verkauf.html">Verkauf</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../Stationsverwaltung/Stationsverwaltung.html">Stationsverwaltung</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="../Geschaeftsfuehrung/reporting.php">Reporting</a>
                </li>
            </ul>
        </nav>

        <div class="content pt-4 text-center">

                <legend class="h1 pb-3">Lagerbestände anzeigen</legend>

                <div class="d-flex p-3">
                    <div class="col-3"></div>
                    <div class="col-3">
                        <label class="col-md-4 control-label mt-3 h4" style="display: block" for="product">Produkt</label>
                    </div>
                    <div class="col-3 mt-3">
                        <select name="product" id="product" class="form-control w-100">
                            <?php
                                // Include the db configuration file
                                require('../includes/config.php');
                
                                $result = $con->query("SELECT * FROM products");
                                while ($rows = $result->fetch_assoc()) {
                                    echo "<option value=" .$rows['ID']. ">" .$rows['name']. "</option>";
                                }
                            ?>
                            <option value="">Alle Produkte</option>
                        </select>
                    </div>
                    <div class="col-3 mt-3">
                        <button onclick="getLagerbestand()" id="getLagerbestand" name="getLagerbestand"
                            class="btn btn-primary">suchen</button>
                    </div>
                </div>


                <div class="d-flex p-3">
                    <div class="col-3"></div>
                    <div class="col-3">
                        <label id="station_label" class="col-md-4 control-label mt-3 h4" for="product"
                            style="display: none">Station</label>
                    </div>
                    <div id="station_select" class="col-3 mt-3" style="display: none">
                        <select name="station" id="station" class="form-control w-100">
                            <?php
                                // Include the db configuration file
                                require('../includes/config.php');
                
                                $result = $con->query("SELECT * FROM station");
                                while ($rows = $result->fetch_assoc()) {
                                    echo "<option value=" .$rows['ID']. ">" .$rows['ID']. "</option>";
                                }
                            ?>
                            <option value="">Alle Stationen</option>
                        </select>
                    </div>
                    <div id="station_button" class="col-3 mt-3" style="display: none">
                        <button onclick="getLagerbestandStation()" id="getLagerbestandStation" name="getLagerbestandStation"
                            class="btn btn-primary">suchen</button>
                    </div>

                    <div class="col-5" id="lagerbestand"></div>
                </div>



            <div class="mx-5 mt-5 pt-5 row">
                <div class="col-6" id="pie">
                </div>
                <div class="col-6" id="bar">
                </div>
            </div>
        </div>

    <script type="text/javascript" src="../js/reporting.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>




</html>
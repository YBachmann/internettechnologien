<?php


class geschaeftsfuehrung {

    // Read 

    public function getLagerbestandProdukt($data) {
        
        // Include the db configuration file
        require('../includes/config.php');
        
        $lagerbestand = array();

        $result = $con->query("SELECT t.Gesamtlagerbestand, (t.Gesamtlagerbestand * (SELECT p.price FROM products p WHERE p.ID = ".$data['productID'].")) as Gesamtwert FROM (SELECT sum(i.currentAmount) as Gesamtlagerbestand FROM inventory i where i.productID = ".$data['productID']." group by i.productID) as t;");

        if (empty($result))
        {
            return "Der Befehls<br>" .$stmt ."<br>ergab keine Treffer<br>".$result;
        }

        while ($row = $result->fetch_assoc()) 
        {
            $lagerbestand[] = $row;
        }

        return  $lagerbestand;
    }

    public function getLagerbestandStation($data) {
        require('../includes/config.php');
        $ret = array();
        $lagerbestand = array();
        $verkäufe = array();

        $result = $con->query("SELECT p.name, sum(i.currentAmount) as lagerbestand FROM inventory i join products p on i.productID = p.ID WHERE i.stationID = ".$data['stationID']." group by i.productID;");
        $result2 = $con->query("SELECT t.name as name, t.anz as anz, (t.anz * t.preis) as wert from
        (select p.name as name, sum(s.amount) as anz, p.price as preis from sales s
        join products p on s.productID = p.ID
        where s.stationID = ".$data['stationID']."
        group by s.productID) as t");

        if (empty($result) || empty($result2))
        {
            return "Der SQL Select ergab keine Treffer!";
        }

        while ($row = $result->fetch_assoc()) 
        {
            $lagerbestand[] = $row;
        }

        while ($row = $result2->fetch_assoc()) {
            $verkäufe[] = $row;
        }

        $ret[0] = $lagerbestand;
        $ret[1] = $verkäufe;

        return  $ret;
    }

    public function getLagerbestandAlle() {

        require('../includes/config.php');
        $ret = array();
        $lagerbestand = array();
        $verkäufe = array();

        $result = $con->query("SELECT p.name, sum(i.currentAmount) as lagerbestand FROM inventory i join products p on i.productID = p.ID group by i.productID;");
        $result2 = $con->query("SELECT t.name as name, t.anz as anz, (t.anz * t.preis) as wert from
        (select p.name as name, sum(s.amount) as anz, p.price as preis from sales s
        join products p on s.productID = p.ID
        group by s.productID) as t");

        if (empty($result) || empty($result2))
        {
            return "Der SQL Select ergab keine Treffer!";
        }

        while ($row = $result->fetch_assoc()) 
        {
            $lagerbestand[] = $row;
        }

        while ($row = $result2->fetch_assoc()) {
            $verkäufe[] = $row;
        }

        $ret[0] = $lagerbestand;
        $ret[1] = $verkäufe;

        return  $ret;
    }
}

?>
<?php

// include necessary classes 
include('geschaeftsfuehrung.php');


$geschaeftsfuehrung = new geschaeftsfuehrung();
$data = array_merge($_GET, $_POST);
$method = $data['action'];

// create SQL based on HTTP method
switch ($method) 
{
    case 'GET':
        if (!empty($data['productID'])) {
            $sql = $geschaeftsfuehrung->getLagerbestandProdukt($data);
            header('Content-type: application/json; charset=utf-8'); 
            echo json_encode($sql); 
            break;
        } else if(!empty($data['stationID'])) {
            $sql = $geschaeftsfuehrung->getLagerbestandStation($data);
            header('Content-type: application/json; charset=utf-8'); 
            echo json_encode($sql); 
            break;
        } else {
            $sql = $geschaeftsfuehrung->getLagerbestandAlle();
            header('Content-type: application/json; charset=utf-8'); 
            echo json_encode($sql);
            break;
        }
}

?>

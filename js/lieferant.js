function showFillup_plan() {
    var url = "../Lieferant/lieferantREST.php";
    var method = "action=GET";
    url += "?" + method + "&fillupplan=true";
    var request = new XMLHttpRequest();
    //alert("url = " + url);
    request.open("GET", url);
    request.onload = function () {
        if (request.status == 200) {
            var databasefilluplist = request.responseText;

            //getTable header for data
            var url2 = "../includes/supplytable.json";
            var request2 = new XMLHttpRequest();
            request2.open("GET", url2);
            request2.onload = function () {
                if (request2.status == 200) {
                    var jsonfilluplist = request2.responseText;
                    //productlist = Response from database || producttable = table from json
                    listtable(databasefilluplist, jsonfilluplist);
                }
            };
            request2.send(null);
        }
    };
    request.send(null);
}

function listtable(databasefilluplist, jsonfilluplist) {
    alert(databasefilluplist + jsonfilluplist);
    var show = document.getElementById("show");
    show.remove();

    var list = document.getElementById("list");
    var databasetable = JSON.parse(databasefilluplist);
    var jsontable = JSON.parse(jsonfilluplist);


    var form = document.createElement("form");
    form.setAttribute("action", "lieferantREST.php");
    form.setAttribute("method", "POST");

    var fieldset = document.createElement("fieldset");

    var hidden = document.createElement("input");
    hidden.setAttribute("type", "hidden");
    hidden.setAttribute("name", "action");
    hidden.setAttribute("value", "POST");

    fieldset.appendChild(hidden);

    var table = document.createElement("table");
    table.id = "filluplist";
    table.classList.add("table", "table-striped", "table-bordered");


    //table head
    var tablehead = document.createElement("thead");
    var tableRow = document.createElement("tr");

    var tableattr = 1;
    for (var h = 0; h < tableattr; h++) {
        var json = jsontable[0]; //in this case only one object exitsts
        var key = "td" + h;
        var tableval = json[key];
        if (tableval != undefined) {
            var tableCell = document.createElement("td");
            var cellContent = document.createTextNode(tableval);
            tableCell.appendChild(cellContent);
            tableRow.appendChild(tableCell);
            tableattr++;
        }
    }
    tablehead.appendChild(tableRow);
    //alert("länge des tableheaders" + tableattr);

    //table body
    var tablebody = document.createElement("tbody");
    var lieferung = 1;
    for (var j = 0; j < databasetable.length; j++) {
        var mycurrentRow = document.createElement("tr");

        tableattr = 1;

        for (var i = 0; i < tableattr; i++) {
            var json = jsontable[0]; // get supplytable.json
            var jsonval = databasetable[j]; // current product from db
            var key = "td" + i; // generate td0, td1, ... for supplytable.json
            var value = json[key]; // get value from supplytable.json
            //alert("key =" + key);
            var tableval = jsonval[value]; // get value from db list with name "value"
            if (tableval != undefined) {

                var mycurrentCell = document.createElement("td");
                var mycurrentText = document.createTextNode(tableval);
                mycurrentCell.appendChild(mycurrentText);
                mycurrentRow.appendChild(mycurrentCell);
                tableattr++;
            } else {
                if (key == "td4") {
                    //berechne auszuliefernde menge
                    //alert("nested if");
                    tableval1 = jsonval["currentAmount"];
                    tableval2 = jsonval["maxAmount"];
                    tosupply = tableval2 - tableval1;
                    //alert(tosupply);
                    var mycurrentCell = document.createElement("td");
                    var mycurrentText = document.createTextNode(tosupply);
                    mycurrentCell.appendChild(mycurrentText);
                    mycurrentRow.appendChild(mycurrentCell);
                    tableattr++;
                } else { // td5
                    //Lieferung abschließen
                    var productTableKeyForName = "td3"; // name
                    var nameValue = jsonval["name"]; // name of the current product
                    var stationIDVAl = jsonval["stationID"];

                    // calculate max value that can be supplied
                    tableval1 = jsonval["currentAmount"];
                    tableval2 = jsonval["maxAmount"];
                    tosupply = tableval2 - tableval1;

                    var mycurrentCell = document.createElement("td");

                    var suppliedAmount = document.createElement("input");
                    suppliedAmount.name = "suppliedAmount" + nameValue + stationIDVAl;
                    suppliedAmount.id = "suppliedAmount" + nameValue + stationIDVAl;
                    suppliedAmount.type = "number";
                    suppliedAmount.setAttribute("max", tosupply);
                    suppliedAmount.setAttribute("min", 0);

                    mycurrentCell.appendChild(suppliedAmount);
                    mycurrentRow.appendChild(mycurrentCell);

                }

            }
            //alert("tableatrr: " + tableattr);

        }
        tablebody.appendChild(mycurrentRow);
        lieferung = lieferung + 1;
    }
    var suppliedbutton = document.createElement("input");
    suppliedbutton.setAttribute("type", "submit");
    suppliedbutton.name = "submitbutton";
    suppliedbutton.value = "Lieferung erfassen";


    table.appendChild(tablehead);
    table.appendChild(tablebody);
    fieldset.appendChild(table);
    fieldset.appendChild(suppliedbutton);
    form.appendChild(fieldset);

    list.appendChild(form);
}
function find_availableProducts() {
    alert("list all available products");
    var url = "../Verkauf/verkaufREST.php";
    var method = "action=GET";
    var stationID = document.getElementById("stationID").value; // Get input value of station id
    var descName = document.getElementById("stationID").name; // Get name of input field (always "StationID")
    url += "?" + method + "&" + descName + "=" + stationID;

    var request = new XMLHttpRequest();
    request.open("GET", url);
    request.onload = function () {
        if (request.status == 200) {
            var productlist = request.responseText; // all products of specified station

            //getTable header for data
            var url2 = "../includes/producttable.json";
            var request2 = new XMLHttpRequest();
            request2.open("GET", url2);
            request2.onload = function () {
                if (request2.status == 200) {
                    var producttable = request2.responseText;
                    //productlist = Response from satabase || producttable = table from json
                    listProducts(stationID, productlist, producttable);
                }
            };
            request2.send(null);


        }
    };
    request.send(null);
}

//productlist = Response from database || getproducttable = table from json
function listProducts(stationID, productlist, getproducttable) {
    alert(productlist + getproducttable);
    var list = document.getElementById("list");
    var products = JSON.parse(productlist);
    var producttable = JSON.parse(getproducttable);


    var form = document.createElement("form");
    form.setAttribute("action", "verkaufREST.php");
    form.setAttribute("method", "POST");


    var buttontodelete = document.getElementById("findproducts");
    buttontodelete.remove();

    var fieldset = document.createElement("fieldset");

    var hidden = document.createElement("input");
    hidden.setAttribute("type", "hidden");
    hidden.setAttribute("name", "action");
    hidden.setAttribute("value", "POST");

    var hiddenstationID = document.createElement("input");
    hiddenstationID.setAttribute("type", "hidden");
    hiddenstationID.setAttribute("value", stationID);
    hiddenstationID.name = "stationID";
    hiddenstationID.setAttribute("id", "stationIDVal");


    fieldset.appendChild(hidden);
    fieldset.appendChild(hiddenstationID);


    var table = document.createElement("table");
    table.id = "productlist";
    table.classList.add("table", "table-striped", "table-bordered");


    //table head
    var tablehead = document.createElement("thead");
    var tableRow = document.createElement("tr");

    var tableattr = 2;
    for (var h = 1; h < tableattr; h++) {
        var json = producttable[0]; //in this case only one object exitsts
        var key = "td" + h;
        var tableval = json[key];
        if (tableval != undefined) {
            var tableCell = document.createElement("td");
            var cellContent = document.createTextNode(tableval);
            tableCell.appendChild(cellContent);
            tableRow.appendChild(tableCell);
            tableattr++;
        }
    }
    tablehead.appendChild(tableRow);
    alert("länge des tableheaders" + tableattr);

    //table body
    var tablebody = document.createElement("tbody");

    alert("produkte= " + products.length)

    for (var j = 0; j < products.length; j++) {
        var mycurrentRow = document.createElement("tr");

        tableattr = 2;
        for (var i = 1; i < tableattr; i++) {
            var json = producttable[0];
            var jsonval = products[j];
            var key = "td" + i;
            var value = json[key];
            var tableval = jsonval[value];
            if (tableval != undefined) {
                var mycurrentCell = document.createElement("td");
                var mycurrentText = document.createTextNode(tableval);
                mycurrentCell.appendChild(mycurrentText);
                mycurrentRow.appendChild(mycurrentCell);
                tableattr++;
            } else {
                // get name
                var productTableKeyForName = "td1";
                var keyForName = json[productTableKeyForName];
                var nameValue = jsonval[keyForName];
                var productID = jsonval["ID"];
                //get current Amount

                var productTableKeyForAmount = "td2";
                var keyForAmount = json[productTableKeyForAmount];
                var amountValue = jsonval[keyForAmount];



                var mycurrentCell = document.createElement("td");
                var inputField = document.createElement("input");
                inputField.name = nameValue;
                inputField.type = "number";
                inputField.id = productID;
                inputField.setAttribute("max", amountValue);
                inputField.setAttribute("min", 0);
                mycurrentCell.appendChild(inputField);
                mycurrentRow.appendChild(mycurrentCell);
            }
        }
        tablebody.appendChild(mycurrentRow);
    }
    table.appendChild(tablehead);
    table.appendChild(tablebody);
    fieldset.appendChild(table);

    var button = document.createElement("button");
    button.innerHTML = "Verkauf erfassen";
    button.setAttribute("type", "submit");

    fieldset.appendChild(button);
    form.appendChild(fieldset);
    list.appendChild(form);
}
function getLagerbestand() {
    var productID = document.getElementById("product").value; // get value from dropdown
    var lagerbestand_div = document.getElementById("lagerbestand");
    var station_select = document.getElementById("station_select");
    var station_button = document.getElementById("station_button");
    var station_label = document.getElementById("station_label");

    if (productID) {
        // Dont show station select dropdown and button if productID is not None
        station_label.style.display = "none";
        station_select.style.display = "none";
        station_button.style.display = "none";

        var url = "geschaeftsfuehrungREST.php";
        var method = 'GET';
        url += "?action=" + method + "&productID=" + productID;

        var request = new XMLHttpRequest();
        request.open("GET", url);
        request.onload = function () {
            if (request.status == 200) {
                var lagerbestand = request.responseText;
                showLagerbestand(lagerbestand);
            }
        };
        request.send(null);
    } else {
        // Show station select dropdown and search button if productID is None (alle Produkte)
        station_label.style.display = "block";
        station_select.style.display = "block";
        station_button.style.display = "block";

        // Tabelle für ein Produkt leeren
        lagerbestand_div.innerHTML = '';
    }
}

function getLagerbestandStation() {
    var stationID = document.getElementById("station").value;
    var url = "geschaeftsfuehrungREST.php";
    var method = 'GET';
    url += "?action=" + method + "&stationID=" + stationID;

    var request = new XMLHttpRequest();
    request.open("GET", url);
    request.onload = function () {
        if (request.status == 200) {
            var lagerbestand = request.responseText;
            showChart(lagerbestand);
        }
    };
    request.send(null);
}

function showLagerbestand(lagerbestand) {
    document.getElementById("pie").innerHTML = '';
    document.getElementById("bar").innerHTML = '';

    var lagerbestand_div = document.getElementById("lagerbestand");
    lagerbestand_div.innerHTML = '';

    var lagerbestand = JSON.parse(lagerbestand);
    var jsonval = lagerbestand[0];
    // Wenn es einen Lagerbestand zum entsprechenden Produkt gibt, zeige es an, sonst Meldung
    if (jsonval) {
        var stock = jsonval['Gesamtlagerbestand'];
        var sum = jsonval['Gesamtwert'];

        var table = document.createElement("table");
        table.classList.add("table", "table-striped", "table-bordered");

        var tablehead = document.createElement("thead");
        var tableRow = document.createElement("tr");
        var th1 = document.createElement("td");
        var th1_context = document.createTextNode("Lagerbestand");
        var th2 = document.createElement("td");
        var th2_context = document.createTextNode("Gesamtwert");
        th1.appendChild(th1_context);
        th2.appendChild(th2_context);
        tableRow.appendChild(th1);
        tableRow.appendChild(th2);
        tablehead.appendChild(tableRow);

        var tablebody = document.createElement("tbody");
        var row = document.createElement("tr");
        var tr1 = document.createElement("td");
        var tr1_context = document.createTextNode(stock);
        var tr2 = document.createElement("td");
        var tr2_context = document.createTextNode(sum + "€");
        tr1.appendChild(tr1_context);
        tr2.appendChild(tr2_context);
        row.appendChild(tr1);
        row.appendChild(tr2);
        tablebody.appendChild(row);

        table.appendChild(tablehead);
        table.appendChild(tablebody);
        lagerbestand_div.appendChild(table);
    } else {
        var h3 = document.createElement("h3");
        var text = document.createTextNode("Produkt wird aktuell nicht angeboten");
        h3.appendChild(text);
        lagerbestand_div.appendChild(h3);
    }
}

function showChart(lagerbestand) {
    // Tabelle für ein Produkt leeren
    document.getElementById("lagerbestand").innerHTML = '';

    // Referenzen aus DOM Baum laden
    var pieDiv = document.getElementById("pie");
    var barDiv = document.getElementById("bar");

    // pie und bar charts leeren, falls sie von früherer auswertung noch gefüllt sind
    pieDiv.innerHTML = '';
    barDiv.innerHTML = '';

    // Cancavs tags erstellen (Zeichenflächen)
    var pieCanvas = document.createElement("canvas");
    var barCanvas = document.createElement("canvas");

    // id hinzufügen
    pieCanvas.setAttribute("id", "pie-chart");
    barCanvas.setAttribute("id", "bar-chart");

    // Zu DOM Baum hinzufügen
    pieDiv.appendChild(pieCanvas);
    barDiv.appendChild(barCanvas);

    var lagerbestand = JSON.parse(lagerbestand)
    var labels_pie = [];
    var labels_bar = [];
    var bestand = [];
    var anzVerkäufe = [];
    var wert = [];

    // lagerbestand[0] = Lagerbestände (alles für pie chart)
    for (i = 0; i < Object.keys(lagerbestand[0]).length; i++) {
        labels_pie.push(lagerbestand[0][i]['name']);
        bestand.push(lagerbestand[0][i]['lagerbestand']);
    }

    // lagerbestand[1] = Verkäufe (alles für bar chart)
    for (i = 0; i < Object.keys(lagerbestand[1]).length; i++) {
        labels_bar.push(lagerbestand[1][i]['name']);
        anzVerkäufe.push(lagerbestand[1][i]['anz']);
        wert.push(lagerbestand[1][i]['wert']);
    }

    new Chart(document.getElementById("pie-chart"), {
        type: 'pie',
        data: {
            labels: labels_pie,
            datasets: [{
                backgroundColor: ["#BF2C23", "#006583", "#0071DE", "#37AAC0", "#E0BD00", "#FF5733", "#4F823F", "#3F5482", "#7F3F82", "#93737B"],
                data: bestand
            }]
        },
        options: {
            title: {
                display: true,
                text: 'Anteile der Produkte am Gesamtlagerbestand'
            }
        }
    });

    new Chart(document.getElementById("bar-chart"), {
        type: 'bar',
        data: {
            labels: labels_bar,
            datasets: [
                {
                    label: "Anzahl der verkauften Produkte",
                    yAxisID: "Anzahl",
                    backgroundColor: ["#BF2C23", "#BF2C23", "#BF2C23", "#BF2C23"],
                    data: anzVerkäufe
                },
                {
                    label: "Wert der verkauften Produkte in €",
                    yAxisID: "Wert",
                    backgroundColor: ["#006583", "#006583", "#006583", "#006583"],
                    data: wert
                }
            ]
        },
        options: {
            legend: { display: true },
            title: {
                display: true,
                text: 'Verkaufsinformationen'
            },
            scales: {
                yAxes: [{
                    id: 'Anzahl',
                    position: 'left',
                    ticks: {
                        min: 0
                    }
                }, {
                    id: 'Wert',
                    position: 'right',
                    ticks: {
                        min: 0
                    }
                }]
            }
        }
    });
}
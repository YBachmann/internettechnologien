<?php
class supply{
   private $db;

   public function __construct()
   {
      //***TODO*** --> insert your database connection:
      $this->db = new mysqli("localhost","allgold", "allgold");

      if (mysqli_connect_errno())
      {
      	die("error while connection to database!:".mysqli_connect_error());
      }

      $this->db->select_db("allgold_erp");

      if($this->db->errno)
      {
      	die ($this->db->error);
      }
   }
   
   public function showFillupPlan(){
       $allfillups = array();
       $stmt = "SELECT A.stationID, B.location, B.type, C.name, A.currentAmount, A.maxAmount  FROM (SELECT * FROM inventory WHERE currentAmount < maxAmount)A LEFT JOIN (SELECT * FROM station) B ON A.stationID = B.ID LEFT JOIN (SELECT * FROM products) C ON A.productID = C.ID ORDER BY A.stationID; ";
       $result = $this->db->query($stmt);

       if(empty($result))
        {
           return "your statement: ".$stmt."<br /> received result:".$result;
        }

      while ($row = $result->fetch_assoc()) 
      {
        $allfillups[] = $row;
      }

      return $allfillups;
        
   }

   public function fillup($data){
      $allproductnames = array();
      $maxstationID = array();

      $stmt = "SELECT ID, name FROM products;";
      $names = $this->db->query($stmt);
      while ($row = $names->fetch_assoc()) 
      {
        $allproductnames[] = $row;
      }
      $stmt = "SELECT MAX(id) as maxStationID FROM station;";
      $higheststationID = $this->db->query($stmt);
      while ($row = $higheststationID->fetch_assoc()) 
      {
        $maxstationID[] = $row;
      }
      $stationIDMax = $maxstationID[0];
      $maxstationIDVAl = $stationIDMax["maxStationID"];
      //go through all stations
      for ($i = 1; $i <= $maxstationIDVAl; $i++)
      {  
         //go through all productnames
         for ($nameiterator = 0; $nameiterator < sizeof($allproductnames); $nameiterator++ ){
            $productnamejson = $allproductnames[$nameiterator];
            $productnameVal = $productnamejson["name"];
            $key = "suppliedAmount" .$productnameVal.'' .$i;
            if (array_key_exists($key, $data)){
               $amount = $data[$key];
               if ($amount > 0){
                  $productID = $productnamejson["ID"];
                  $stationID = $i;
                  $stmt = "INSERT INTO refill (productID, stationID, supplierID, amount) VALUES ('".$productID."', '".$stationID."', 3, '".$amount."');";
                  $this->db->query($stmt);
                  $stmt = "UPDATE inventory SET currentAmount = currentAmount + '".$amount."' WHERE productID = '".$productID."' AND stationID = '".$stationID."';";
                  $this->db->query($stmt);
               }
            }
         }
         
      }
      return ("Lieferung erfolgreich registriert");
   }
}
?>
-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 18. Jul 2020 um 11:00
-- Server-Version: 10.4.11-MariaDB
-- PHP-Version: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `allgold_erp`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `inventory`
--

CREATE TABLE `inventory` (
  `ID` int(11) NOT NULL,
  `stationID` int(10) NOT NULL,
  `productID` int(10) NOT NULL,
  `currentAmount` int(3) NOT NULL,
  `maxAmount` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `inventory`
--

INSERT INTO `inventory` (`ID`, `stationID`, `productID`, `currentAmount`, `maxAmount`) VALUES
(1, 13, 2, 5, 30),
(2, 13, 1, 41, 50),
(4, 1, 1, 35, 50),
(5, 2, 1, 5, 50),
(6, 13, 3, 25, 80);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `products`
--

CREATE TABLE `products` (
  `ID` int(10) NOT NULL,
  `name` varchar(20) NOT NULL,
  `price` decimal(3,2) NOT NULL,
  `durability` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `products`
--

INSERT INTO `products` (`ID`, `name`, `price`, `durability`) VALUES
(1, 'Milch', '1.00', 14),
(2, 'Emmentaler', '3.95', 60),
(3, 'Gauda', '3.10', 60),
(4, 'Joghurt 100g', '0.50', 7),
(5, 'Quark', '0.90', 10),
(6, 'Joghurt 500g', '2.00', 7),
(7, 'Streichkaese', '1.50', 21),
(8, 'Bergkaese', '5.00', 60);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `refill`
--

CREATE TABLE `refill` (
  `ID` int(11) NOT NULL,
  `productID` int(10) NOT NULL,
  `stationID` int(10) NOT NULL,
  `supplierID` int(10) NOT NULL,
  `amount` int(3) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `refill`
--

INSERT INTO `refill` (`ID`, `productID`, `stationID`, `supplierID`, `amount`, `timestamp`) VALUES
(1, 1, 13, 3, 5, '2020-05-16 10:50:49'),
(2, 1, 13, 3, 5, '2020-05-16 10:51:20'),
(3, 1, 13, 3, 5, '2020-05-16 10:51:44'),
(4, 1, 13, 3, 5, '2020-05-16 10:52:38'),
(5, 1, 13, 3, 15, '2020-05-16 11:18:37'),
(6, 1, 13, 3, 5, '2020-05-16 11:41:49'),
(7, 1, 13, 3, 5, '2020-05-16 11:42:22'),
(8, 1, 13, 3, 5, '2020-05-16 12:04:08'),
(9, 1, 13, 3, 5, '2020-05-19 15:43:17'),
(10, 2, 13, 3, 100, '2020-05-19 15:43:18'),
(12, 2, 13, 3, 80, '2020-05-19 15:44:33'),
(14, 1, 13, 3, -5, '2020-05-19 15:45:36'),
(15, 2, 13, 3, 20, '2020-05-19 16:22:22'),
(16, 1, 13, 3, 20, '2020-05-19 16:27:16'),
(17, 1, 13, 3, 5, '2020-05-19 16:49:17'),
(20, 3, 13, 3, 50, '2020-05-21 10:45:32'),
(21, 1, 13, 3, 5, '2020-05-27 11:22:41'),
(22, 1, 13, 3, 5, '2020-05-27 11:22:55'),
(23, 1, 13, 3, 7, '2020-05-27 11:28:07'),
(24, 1, 13, 3, 80, '2020-05-27 11:39:25'),
(27, 3, 13, 3, 10, '2020-06-30 13:14:37'),
(28, 3, 13, 3, 10, '2020-06-30 13:15:15'),
(29, 3, 13, 3, 5, '2020-06-30 13:22:43');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `roles`
--

CREATE TABLE `roles` (
  `ID` int(11) NOT NULL,
  `name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `roles`
--

INSERT INTO `roles` (`ID`, `name`) VALUES
(1, 'Geschäftsführer'),
(2, 'Verkäufer'),
(3, 'Lieferant');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `sales`
--

CREATE TABLE `sales` (
  `ID` int(11) NOT NULL,
  `productID` int(10) NOT NULL,
  `stationID` int(10) NOT NULL,
  `userID` int(11) NOT NULL,
  `amount` int(3) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `sales`
--

INSERT INTO `sales` (`ID`, `productID`, `stationID`, `userID`, `amount`, `timestamp`) VALUES
(8, 1, 13, 3, 5, '2020-05-13 12:31:21'),
(9, 1, 13, 3, 5, '2020-05-13 12:31:37'),
(11, 1, 13, 3, 5, '2020-05-15 14:44:09'),
(12, 1, 13, 3, 5, '2020-05-15 14:44:13'),
(13, 1, 13, 3, 5, '2020-05-15 14:52:53'),
(14, 1, 13, 3, 5, '2020-05-15 14:53:14'),
(18, 1, 13, 3, 5, '2020-05-15 14:55:42'),
(19, 1, 13, 3, 5, '2020-05-15 15:06:31'),
(20, 1, 13, 3, 5, '2020-05-15 15:08:01'),
(21, 1, 13, 3, 5, '2020-05-15 15:08:13'),
(22, 1, 13, 3, 5, '2020-05-15 15:08:16'),
(23, 1, 13, 3, 5, '2020-05-15 15:11:23'),
(24, 1, 13, 3, 5, '2020-05-15 15:11:27'),
(25, 1, 13, 3, 5, '2020-05-15 15:12:02'),
(26, 1, 13, 3, 5, '2020-05-15 15:12:22'),
(27, 1, 13, 3, 5, '2020-05-15 15:13:01'),
(28, 1, 13, 3, 5, '2020-05-15 15:13:08'),
(29, 1, 13, 3, 5, '2020-05-15 15:14:35'),
(30, 1, 13, 3, 5, '2020-05-15 15:18:10'),
(31, 1, 13, 3, 5, '2020-05-15 15:18:59'),
(32, 1, 13, 3, 5, '2020-05-15 15:19:14'),
(33, 1, 13, 3, 5, '2020-05-15 15:28:37'),
(34, 1, 13, 3, 5, '2020-05-15 15:38:44'),
(35, 1, 13, 3, 5, '2020-05-15 15:42:08'),
(36, 1, 13, 3, 5, '2020-05-15 15:42:36'),
(37, 1, 13, 3, 5, '2020-05-15 15:42:39'),
(38, 1, 13, 3, 5, '2020-05-15 15:42:40'),
(40, 1, 13, 3, 5, '2020-05-16 08:22:44'),
(41, 1, 13, 3, 5, '2020-05-16 08:23:44'),
(42, 1, 13, 3, 5, '2020-05-16 08:24:28'),
(43, 1, 13, 3, 5, '2020-05-16 08:25:42'),
(44, 1, 13, 3, 5, '2020-05-16 08:26:15'),
(45, 1, 13, 3, 5, '2020-05-16 08:26:16'),
(46, 1, 13, 3, 5, '2020-05-16 08:26:53'),
(47, 1, 13, 3, 5, '2020-05-16 08:26:55'),
(48, 1, 13, 3, 5, '2020-05-16 08:27:36'),
(49, 1, 13, 3, 5, '2020-05-16 08:27:37'),
(50, 1, 13, 3, 5, '2020-05-16 08:30:22'),
(51, 1, 13, 3, 5, '2020-05-16 08:30:50'),
(52, 1, 13, 3, 5, '2020-05-16 08:34:30'),
(53, 1, 13, 3, 5, '2020-05-16 08:34:33'),
(54, 1, 13, 3, 5, '2020-05-16 08:34:35'),
(55, 1, 13, 3, 5, '2020-05-16 08:36:03'),
(56, 1, 13, 3, 5, '2020-05-16 08:36:50'),
(57, 1, 13, 3, 5, '2020-05-16 08:36:56'),
(58, 1, 13, 3, 5, '2020-05-16 08:37:07'),
(59, 1, 13, 3, 5, '2020-05-16 08:37:50'),
(65, 1, 13, 2, 5, '2020-05-16 09:11:16'),
(66, 1, 13, 2, 5, '2020-05-16 09:29:45'),
(67, 1, 13, 2, 5, '2020-05-16 09:54:42'),
(68, 1, 13, 2, 5, '2020-05-16 10:04:10'),
(69, 1, 13, 2, 5, '2020-05-16 10:04:23'),
(70, 1, 13, 2, 5, '2020-05-16 10:05:33'),
(71, 1, 13, 2, 5, '2020-05-16 10:08:29'),
(72, 1, 13, 2, 5, '2020-05-16 10:08:45'),
(73, 1, 13, 2, 5, '2020-05-16 10:13:01'),
(74, 1, 13, 2, 5, '2020-05-16 10:52:01'),
(75, 1, 13, 2, 5, '2020-05-16 10:52:09'),
(76, 1, 13, 2, 5, '2020-05-16 10:52:15'),
(77, 1, 13, 2, 5, '2020-05-16 11:15:34'),
(78, 1, 13, 2, 5, '2020-05-16 12:02:34'),
(79, 1, 13, 2, 5, '2020-05-19 09:36:39'),
(80, 1, 13, 2, 5, '2020-05-19 09:36:49'),
(81, 2, 13, 2, 200, '2020-05-19 16:21:10'),
(83, 1, 13, 3, 20, '2020-05-19 16:24:55'),
(84, 2, 13, 3, 20, '2020-05-19 16:25:06'),
(85, 2, 13, 3, 20, '2020-05-19 16:25:57'),
(87, 2, 13, 3, 5, '2020-05-19 16:29:06'),
(88, 2, 13, 3, 5, '2020-05-19 16:29:54'),
(89, 2, 13, 3, 5, '2020-05-19 16:30:14'),
(92, 2, 13, 3, 5, '2020-05-19 16:31:14'),
(94, 2, 13, 3, 5, '2020-05-19 16:31:39'),
(100, 2, 13, 3, 5, '2020-05-19 16:35:53'),
(101, 2, 13, 3, 5, '2020-05-19 16:36:13'),
(102, 2, 13, 3, 5, '2020-05-19 16:36:31'),
(103, 2, 13, 3, 5, '2020-05-19 16:36:41'),
(104, 2, 13, 3, 5, '2020-05-19 16:36:48'),
(105, 2, 13, 3, 5, '2020-05-19 16:36:57'),
(106, 2, 13, 3, 5, '2020-05-19 16:37:20'),
(107, 2, 13, 3, 5, '2020-05-19 16:38:07'),
(111, 2, 13, 3, 5, '2020-05-19 16:39:31'),
(115, 2, 13, 3, 2, '2020-05-19 16:41:52'),
(116, 2, 13, 3, 3, '2020-05-19 16:41:59'),
(118, 2, 13, 2, 5, '2020-05-19 16:50:52'),
(120, 1, 13, 2, 5, '2020-05-19 17:41:24'),
(121, 1, 13, 2, 5, '2020-05-19 17:42:24'),
(122, 1, 13, 2, 5, '2020-05-19 17:44:14'),
(123, 1, 13, 2, 5, '2020-05-19 17:44:39'),
(124, 1, 13, 2, 5, '2020-05-19 17:44:52'),
(127, 1, 13, 2, 60, '2020-05-27 11:39:36'),
(128, 1, 13, 2, 5, '2020-05-27 11:39:46'),
(129, 3, 13, 2, 10, '2020-06-11 09:33:31'),
(130, 3, 13, 2, 5, '2020-06-30 13:31:40'),
(131, 3, 13, 2, 5, '2020-07-01 10:53:44');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `station`
--

CREATE TABLE `station` (
  `ID` int(10) NOT NULL,
  `coordsA` varchar(30) NOT NULL,
  `coordsL` varchar(30) NOT NULL,
  `location` varchar(30) NOT NULL,
  `type` varchar(1) NOT NULL,
  `description` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `station`
--

INSERT INTO `station` (`ID`, `coordsA`, `coordsL`, `location`, `type`, `description`) VALUES
(1, '47.71457225467146', '10.314338207244873', 'Kempten', 'B', 'Firmensitz'),
(2, '47.72426740349347', '10.316848754882812', 'Kempten', 'A', 'Außenstelle'),
(3, '1234.4312', '1234.4321', 'Aheeeeeeeeeg', 'A', 'aaaaaaaaaaaaa'),
(4, '47.98036221803081', '10.1788330078125', 'Memmingen', 'B', 'Außenstelle'),
(5, '47.694697038966076', '10.038070678710938', 'Isny', 'A', 'Außenstelle'),
(6, '47.77941861197757', '10.616891384124756', 'Marktoberdorf', 'A', 'Außenstelle'),
(7, '47.514634612973694', '10.26755619153846', 'Sonthofen', 'V', 'Außenstelle'),
(8, '47.41029678060909', '10.275293827580754', 'Oberstdorf', 'A', 'Außenstelle'),
(9, '47.554643912647045', '10.022393703984562', 'Oberstaufen', 'A', 'Außenstelle'),
(10, '47.560841627466885', '10.21770143561298', 'Immenstadt', 'A', 'Außenstelle'),
(11, '47.569648', '10.700432800000044', 'Füssen', 'B', 'Außenstelle'),
(12, '47.550241351721596', '9.69220304476039', 'Lindau', 'B', 'Außenstelle'),
(13, '25.25466225', '36.12585', 'Kempten', 'V', 'Teststelle'),
(15, '25.65236952', '12.2541252', 'Kaufbeuren', 'V', 'Teststelle 2');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `users`
--

CREATE TABLE `users` (
  `ID` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(100) NOT NULL,
  `roleID` int(1) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `users`
--

INSERT INTO `users` (`ID`, `username`, `password`, `roleID`, `firstname`, `lastname`) VALUES
(1, 'GF', '1d0b32c3430879c653362eaacb1c8d1d', 1, 'Max', 'Mustermann'),
(2, 'V', '34d3ed45b0a36c2e88f1742264d7eddb', 2, 'Florian', 'Fuchs'),
(3, 'L', '9a55aa43f028f00f4fef6607debf8786', 3, 'Antonia', 'Amann');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_inventory_station` (`stationID`),
  ADD KEY `fk_inventory_products` (`productID`);

--
-- Indizes für die Tabelle `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `refill`
--
ALTER TABLE `refill`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_refill_station` (`stationID`),
  ADD KEY `fk_refill_product` (`productID`),
  ADD KEY `fk_refill_users` (`supplierID`);

--
-- Indizes für die Tabelle `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_sales_station` (`stationID`),
  ADD KEY `fk_sales_products` (`productID`),
  ADD KEY `fk_sales_users` (`userID`);

--
-- Indizes für die Tabelle `station`
--
ALTER TABLE `station`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_users_roles` (`roleID`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `inventory`
--
ALTER TABLE `inventory`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT für Tabelle `products`
--
ALTER TABLE `products`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT für Tabelle `refill`
--
ALTER TABLE `refill`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT für Tabelle `roles`
--
ALTER TABLE `roles`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT für Tabelle `sales`
--
ALTER TABLE `sales`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=132;

--
-- AUTO_INCREMENT für Tabelle `station`
--
ALTER TABLE `station`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT für Tabelle `users`
--
ALTER TABLE `users`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `inventory`
--
ALTER TABLE `inventory`
  ADD CONSTRAINT `fk_inventory_products` FOREIGN KEY (`productID`) REFERENCES `products` (`ID`),
  ADD CONSTRAINT `fk_inventory_station` FOREIGN KEY (`stationID`) REFERENCES `station` (`ID`);

--
-- Constraints der Tabelle `refill`
--
ALTER TABLE `refill`
  ADD CONSTRAINT `fk_refill_product` FOREIGN KEY (`productID`) REFERENCES `products` (`ID`),
  ADD CONSTRAINT `fk_refill_station` FOREIGN KEY (`stationID`) REFERENCES `station` (`ID`),
  ADD CONSTRAINT `fk_refill_users` FOREIGN KEY (`supplierID`) REFERENCES `users` (`ID`);

--
-- Constraints der Tabelle `sales`
--
ALTER TABLE `sales`
  ADD CONSTRAINT `fk_sales_products` FOREIGN KEY (`productID`) REFERENCES `products` (`ID`),
  ADD CONSTRAINT `fk_sales_station` FOREIGN KEY (`stationID`) REFERENCES `station` (`ID`),
  ADD CONSTRAINT `fk_sales_users` FOREIGN KEY (`userID`) REFERENCES `users` (`ID`);

--
-- Constraints der Tabelle `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_users_roles` FOREIGN KEY (`roleID`) REFERENCES `roles` (`ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

set path="C:\xampp\mysql\bin\"

REM # Daten aus der Datenbank holen
mysqldump --xml -u allgold -pallgold -h localhost allgold_erp > dbkatalog.xml

REM # XSL-Transformation durchführen
java -cp ..\SaxonEE10-1J\saxon-ee-10.1.jar net.sf.saxon.Transform -o:katalog.fo dbkatalog.xml katalog.xsl

REM # FOP Transformation
..\fop-2.5\fop\fop katalog.fo katalog.pdf

REM # erstellte PDF Anzeigen
start katalog.pdf
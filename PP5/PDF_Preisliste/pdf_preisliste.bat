REM # XSL-Transformation durchführen
java -cp ..\SaxonEE10-1J\saxon-ee-10.1.jar net.sf.saxon.Transform -o:preisliste.fo ../products.xml preisliste.xsl

REM # FOP Transformation
..\fop-2.5\fop\fop .\preisliste.fo .\preisliste.pdf

REM # erstellte PDF Anzeigen
start preisliste.pdf